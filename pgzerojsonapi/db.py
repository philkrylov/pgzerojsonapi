import asyncpg  # type: ignore
from databases import Database
from databases.backends.postgres import PostgresBackend

db = None
db_timezone = None


class TimeZonedPostgresBackend(PostgresBackend):
    async def connect(self) -> None:
        assert self._pool is None, "DatabaseBackend is already running"
        kwargs = self._get_connection_kwargs()
        self._pool = await asyncpg.create_pool(str(self._database_url),
                                               server_settings={'timezone': db_timezone},
                                               **kwargs)


Database.SUPPORTED_BACKENDS["postgresql"] = "pgzerojsonapi.db:TimeZonedPostgresBackend"


def db_init_app(url: str,
                min_size: int,
                max_size: int,
                timezone,
                app):
    global db
    global db_timezone
    db = Database(url, min_size=min_size, max_size=max_size)
    db_timezone = timezone

    @app.on_event("startup")
    async def startup():
        await db.connect()

    @app.on_event("shutdown")
    async def shutdown():
        await db.disconnect()
