from typing import Any, Callable, Dict, Optional, Tuple

import django.apps  # type: ignore
from django.core import validators as django_validators  # type: ignore
from django.db.models import (  # type: ignore
    DateField,
    DateTimeField,
    IntegerField,
    PositiveIntegerField,
    UUIDField,
)
import sqlalchemy


def get_one_to_many_description(model, name):
    return getattr(model, 'AUTO_ONE_TO_MANY_FIELDS', {}).get(name)


def reflect_django_models(
    metadata: sqlalchemy.MetaData,
    rename_models: Dict[str, str] = {},
    renamer: Callable[[str], str] = None,
    describe_relationships: Dict[str, Dict[str, str]] = {},
) -> Tuple[Dict[str, Dict[str, Tuple[str, str, Optional[str]]]],
           Dict[str, Dict[str, Tuple[str, str, str, str, str, str, str]]]]:
    one_to_many_rels = {}
    many_to_many_rels = {}
    inspector = sqlalchemy.engine.reflection.Inspector.from_engine(metadata.bind)
    for model in django.apps.apps.get_models():
        meta = model._meta
        name = rename_models.get(meta.label)
        if not name:
            name = '.'.join(meta.label.rsplit('.', maxsplit=2)[-2:])
            if renamer:
                name = renamer(name)
        if not meta.db_table or meta.abstract:
            continue
        inspected_cols = {c['name']: c for c in inspector.get_columns(meta.db_table)}
        pks = inspector.get_primary_keys(meta.db_table)
        columns = []  # type: ignore
        for f in meta.get_fields():
            if f.concrete:
                field_validators: Dict[str, Any] = {}
                if f.choices:
                    field_validators['enum'] = [s for s, _ in f.choices]
                if hasattr(f, 'decimal_places'):
                    field_validators['multipleOf'] = 10 ** -f.decimal_places
                    field_validators['format'] = 'decimal'
                if isinstance(f, IntegerField):
                    if f.choices:
                        field_validators['maximum'] = max(*field_validators['enum'])
                        field_validators['minimum'] = min(*field_validators['enum'])
                    else:
                        field_validators['maximum'] = 2 ** 31 - 1
                        field_validators['minimum'] = (0 if isinstance(f, PositiveIntegerField)
                                                       else -2 ** 31)
                if isinstance(f, UUIDField):
                    field_validators['format'] = 'uuid'
                for validator in f.validators:
                    if hasattr(validator, 'regex'):
                        field_validators['pattern'] = validator.regex.pattern
                    elif f.choices or isinstance(f, (DateField, DateTimeField)):
                        pass
                    elif isinstance(validator, django_validators.MaxValueValidator):
                        field_validators['maximum'] = validator.limit_value
                    elif isinstance(validator, django_validators.MinValueValidator):
                        field_validators['minimum'] = validator.limit_value
                if field_validators:
                    icol = inspected_cols[f.attname]
                    for k in ('type',):
                        icol[k + '_'] = icol.pop(k)
                    columns.append(sqlalchemy.Column(**icol,
                                                     primary_key=(f.attname in pks),
                                                     info=field_validators))  # type: ignore

        one_to_many_rels[meta.db_table] = {
            f.name: (f.field.model._meta.db_table,
                     f.field.attname,
                     (get_one_to_many_description(model, f.name) or
                      describe_relationships.get(meta.db_table, {}).get(f.name)))
            for f in meta.get_fields()
            if f.auto_created and not f.concrete and f.one_to_many
        }
        many_to_many_rels[meta.db_table] = {
            f.name: (f.verbose_name, f.m2m_db_table(), f.m2m_column_name(), f.m2m_reverse_name(),
                     f.related_model._meta.db_table, f.related_model._meta.pk.column,
                     f.related_query_name())
            for f in meta.get_fields()
            if not f.auto_created and f.many_to_many
        }
        for _, thru_table, _, _, target_table, _, _ in many_to_many_rels[meta.db_table].values():
            sqlalchemy.Table(thru_table, metadata, autoload=True)
            sqlalchemy.Table(target_table, metadata, autoload=True)
        sqlalchemy.Table(meta.db_table, metadata,
                         *columns,
                         info=dict(title=name),
                         autoload=True,
                         extend_existing=True)
    return one_to_many_rels, many_to_many_rels
