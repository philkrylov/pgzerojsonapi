import base64
import dataclasses
import datetime
from functools import lru_cache
from gettext import gettext as T_
from itertools import chain
import operator
import re
import struct
import typing
from typing import Any, AsyncGenerator, AsyncIterable, Awaitable, Callable, Dict, Generic, \
        Hashable, Iterable, List, Optional, Sequence, Tuple, Type, TypeVar, Union
from urllib.parse import parse_qsl, urlencode
import uuid

import fastapi
import orjson as json
from pydantic import BaseModel, Field
from pydantic.fields import FieldInfo
from sqlalchemy import column, func, literal_column, tuple_
from sqlalchemy.sql import bindparam, exists, select
from sqlalchemy.sql.expression import CTE, ColumnElement, FromClause, Select, Tuple as Tuple_, and_
from sqlalchemy.sql.operators import ColumnOperators
import starlette
import temporenc  # type: ignore

from pgzerojson import \
    PgZeroJsonConfig, TableCTE, get_python_type, get_referenced_by, select_json_sql

from .db import db


if typing.TYPE_CHECKING:
    T = TypeVar('T')

    class ListResponse(Generic[T], BaseModel):
        count: int = Field(..., title=T_("Total results count"))
        results: List[T] = Field(..., title=T_("A page of results"))  # type: ignore
        next: str = Field(None, title=T_("Next page link"))
        previous: str = Field(None, title=T_("Previous page link"))
else:
    names_registered = set()

    def list_response_class_factory(item_type: Type[BaseModel]) -> Type[BaseModel]:
        class ListResponse_(BaseModel):
            count: int = Field(..., title=T_("Total results count"))
            results: List[item_type] = Field(..., title=T_("A page of results"))  # type: ignore
            next: str = Field(None, title=T_("Next page link"))
            previous: str = Field(None, title=T_("Previous page link"))
        name = canonical_name = f"{item_type.__name__}_ListResponse"
        n = 0
        while name in names_registered:
            n += 1
            name = f"{canonical_name}{n}"
        names_registered.add(name)
        ListResponse_.__name__ = ListResponse_.__qualname__ = name
        return ListResponse_


def make_sort_param(config: PgZeroJsonConfig, default_desc: bool = False) -> fastapi.params.Query:
    choices = [_.name for _ in config.order_keys]
    regex = "|".join(choices)
    return fastapi.params.Query(("-" if default_desc else "") + choices[0],
                                title=T_("Sort by field"),
                                description=T_("Starting `-` character sorts in descending order"),
                                regex=f"^|-?(?:{regex})$")


@dataclasses.dataclass
class CommonParams:
    cursor: bytes = fastapi.Query(None, title=T_("Page cursor"), min_length=24, max_length=40)
    limit: int = fastapi.Query(50, title=T_("Max page length in `offset` pagination mode"),
                               min_value=1, max_value=500, deprecated=True)
    offset: int = fastapi.Query(0, title=T_("Offset from the start in `offset` pagination mode"),
                                min_value=0, deprecated=True)
    page_size: int = fastapi.Query(50, title=T_("Max page length"),
                                   min_value=1, max_value=500)
    pager: str = fastapi.Query('cursor', title=T_("Pagination mode (`cursor` or `offset`)"),
                               regex="^(?:cursor|offset)$", deprecated=True)
    sort: str = None  # type: ignore
    slow: bool = fastapi.Query(False, title=T_("Slow mode"))
    format: str = fastapi.Query("json", title=T_("Response format"), regex=r"json$")


@dataclasses.dataclass
class NDJSONFormatParams(CommonParams):
    format: str = fastapi.Query("json", title=T_("Response format"), regex=r"(?:json|ndjson)$")


class FilterParams:
    def __hash__(self) -> int:
        return hash((type(self),) + dataclasses.astuple(self))

    def dict(self, exclude_none: bool = False) -> Dict[str, Any]:
        d = dataclasses.asdict(self)
        if exclude_none:
            return dict((k, v) for k, v in d.items()
                        if v is not None and not isinstance(v, fastapi.params.Query))
        return d

    def keys_set(self):
        return tuple(sorted(self.dict(exclude_none=True).keys()))


class Op(BaseModel):
    op: Callable
    description: str
    parse: Optional[Callable[[Any], Any]] = None
    expr: Optional[Callable[[ColumnElement, str], ColumnElement]] = None

    def parse_param(self, s: str):
        return self.parse(s) if self.parse else s

    def make_expr(self, column: ColumnElement, value: str) -> ColumnElement:
        return self.expr(column, value) if self.expr else self.op(column, bindparam(value))


OPS = {
    "exact": Op(op=operator.eq, description=T_("exactly equals")),
    "gt": Op(op=operator.gt, description=T_("greater than")),
    "gte": Op(op=operator.ge, description=T_("greater or equal")),
    "in": Op(op=operator.eq, description=T_("equals any of"),
             parse=(lambda v: v[0].split(',')),
             expr=(lambda c, v: c == func.any(bindparam(v)))),
    "isnull": Op(op=operator.eq, description=T_("is null"),
                 expr=(lambda c, v: c.is_(None) == bool(v))),
    "lt": Op(op=operator.lt, description=T_("less than")),
    "lte": Op(op=operator.le, description=T_("less or equal")),
    "ne": Op(op=operator.ne, description=T_("is not equal")),
    "range": Op(op=operator.eq, description=T_("is between"),
                expr=(lambda c, v: c.between(*v))),
    "startswith": Op(op=ColumnOperators.startswith, description=T_("starts with"),
                     expr=(lambda c, v: c.startswith(bindparam(v)))),
    "contains": Op(op=ColumnOperators.contains, description=T_("contains substring"),
                   expr=(lambda c, v: c.contains(bindparam(v)))),
    "customlist": Op(op=operator.contains, description=T_("comma-separated list")),
}


cached_counts = {}


def apply_keyset(query: Select,
                 keyset: Tuple_,
                 order_key: Tuple_,
                 desc: bool,
                 order_key_types: Sequence[type]) -> Select:
    op = (operator.lt if desc else operator.gt)
    return query.where(op(order_key, keyset))


def apply_offset(query: Select,
                 offset: int) -> Select:
    return query.offset(offset)


def make_filter_name(name: str, op: str) -> str:
    return (name if op in ("exact", "custom", "customlist")
            else "__".join((name, op)))


def filter_factory(
    name: str,
    *fields: Tuple[str, type, Optional[FieldInfo], str, Sequence[str]],
) -> type:
    klass = type(
        name,
        (FilterParams,),
        dict(chain.from_iterable(
            ((make_filter_name(name, op),
              fastapi.Query(def_value,
                            title=": ".join((description, OPS[op].description)),
                            description=": ".join((description, OPS[op].description))))
             for op in ops)
            for name, _, def_value, description, ops in fields
        )),
    )
    klass.__annotations__ = dict(chain.from_iterable(
        ((make_filter_name(name, op),
            Sequence[type_] if op in ('in', 'customlist') else type_)  # type: ignore
         for op in ops)
        for name, type_, _, _, ops in fields
    ))
    return dataclasses.dataclass(klass)


def get_count_cache_key(location) -> str:
    return str(location.replace(query=querystring_replace(
        location.query,
        cursor=None,
        offset=None,
        page_size=None,
        limit=None,
        pager=None,
        slow=None,
        sort=None,
    )))


def get_keyset(value: Dict[str, Any], order_keys: Sequence[str]) -> tuple:
    return tuple(value[k.lstrip('-')] for k in order_keys)


def get_page_url(location: starlette.datastructures.URL,
                 cursor: bytes = None,
                 pager: Optional[str] = 'cursor',
                 offset: int = 0) -> str:
    return str(location.replace(query=querystring_replace(
        location.query,
        **(dict(cursor=cursor) if cursor
           else dict(offset=b'%u' % offset)),
    )))


PgZeroJsonJoin = Union[
    Tuple[TableCTE, str, str],
    Tuple[TableCTE, str, Callable[[FromClause, FromClause], ColumnElement]],
]
PgZeroJsonJoins = Dict[TableCTE, List[PgZeroJsonJoin]]
CustomFilter = Callable[
    [TableCTE],
    Tuple[Dict[Sequence[str], List[ColumnElement]],
          PgZeroJsonJoins,
          PgZeroJsonJoins],
]


def dict_of_lists_extend(dict1: Dict[Any, List], dict2: Dict[Any, List]):
    for k, v in dict2.items():
        dict1.setdefault(k, []).extend(v)


def get_query_factory(config: PgZeroJsonConfig,
                      custom_filters: Dict[str, CustomFilter]) -> Callable:
    @lru_cache
    def get_query(where_params):
        noncustom_where_params = []
        where_clauses, joins, extra_joins = {}, {}, {}
        for where_param in where_params:
            if where_param in custom_filters:
                custom_filter = custom_filters[where_param]
                this_wheres, this_joins, this_extra_joins = custom_filter(config.table)
                dict_of_lists_extend(where_clauses, this_wheres)
                dict_of_lists_extend(joins, this_joins)
                dict_of_lists_extend(extra_joins, this_extra_joins)
            else:
                noncustom_where_params.append(where_param)
        this_wheres, this_joins, this_extra_joins = \
            parse_where(noncustom_where_params, config.table, config)
        dict_of_lists_extend(where_clauses, this_wheres)
        dict_of_lists_extend(joins, this_joins)
        dict_of_lists_extend(extra_joins, this_extra_joins)

        count_q = select([func.count()]).select_from(make_join(config.table, joins | extra_joins)
                                                     if joins or extra_joins else config.table)
        for whereclauses in where_clauses.values():
            for whereclause in whereclauses:
                count_q = count_q.where(whereclause)

        add_inner_joins: PgZeroJsonJoins = config.add_inner_joins.copy()
        for t, joinlist in extra_joins.items():
            add_inner_joins[t] = add_inner_joins.get(t, []) + joinlist

        q = select_json_sql(table=config.table,
                            cfg=config.replace(add_inner_joins=add_inner_joins),
                            order_keys=set(chain(config.order_keys,
                                                 (config.table.c[parse_lookup(_)[0]]
                                                  for _ in ()))),
                            where=where_clauses)  # where_params

        return count_q, q

    return get_query


def keyset_deserialize(value: bytes, types: Iterable[type]) -> tuple:
    result = []
    value = base64.b64decode(value, b'-_')
    for type_ in types:
        if type_ is datetime.datetime:
            moment = temporenc.unpackb(value[:9])
            result.append(moment.datetime() if moment.year else None)
            value = value[9:]
        elif type_ is datetime.date:
            moment = temporenc.unpackb(value[:3])
            result.append(moment.date() if moment.year else None)
            value = value[3:]
        elif type_ is int:
            number = struct.unpack('q', value[:8])[0]
            result.append(number if number != -2**63 else None)
            value = value[8:]
        elif issubclass(type_, uuid.UUID):
            raw_uuid = value[:16]
            result.append(uuid.UUID(bytes=raw_uuid) if raw_uuid != b'\x00' * 16 else None)
            value = value[16:]
        elif type_ is str:
            length = value[0]
            if length == b'\xFF':
                result.append(None)
                value = value[1:]
            else:
                result.append(value[1:(1 + length)].decode())
                value = value[1 + length:]
        else:
            raise TypeError
    return tuple(result)


def keyset_serialize(value: tuple, types: Iterable[type]) -> bytes:
    result = []
    for t, v in zip(types, value):
        if type(v) is datetime.datetime:
            result.append(temporenc.packb(v, type="DTSZ"))
        elif type(v) is datetime.date:
            result.append(temporenc.packb(v, type="D"))
        elif type(v) is int:
            result.append(struct.pack('q', v))
        elif isinstance(v, uuid.UUID):
            result.append(v.bytes)
        elif type(v) is str:
            v_bytes = v.encode()
            result.append(b'%c%s' % (len(v_bytes), v_bytes))
        elif v is None:
            if t is datetime.datetime:
                result.append(temporenc.packb(type="DTSZ", microsecond=0, tz_offset=0))
            elif t is datetime.date:
                result.append(temporenc.packb(type="D"))
            elif t is int:
                result.append(struct.pack('q', -2**63))
            elif t is uuid.UUID:
                result.append(b'\x00' * 16)
            elif t is str:
                result.append(b'\xFF')
            else:
                raise TypeError(f"I don't know how to serialize None in {t} context in {value}")
        else:
            raise TypeError(f"I don't know how to serialize {type(v)} in {value}")
    return base64.b64encode(b''.join(result), b'-_')


def min_value(t: type) -> ColumnElement:
    if t is datetime.datetime:
        return literal_column("'-infinity'::timestamptz")
    elif t is int:
        return literal_column("-9223372036854775808")
    raise TypeError(f"I don't know how to make minimal value of type {t} in SQL")


async def ndjson_result(result: AsyncIterable[Any]) -> AsyncGenerator[str, None]:
    async for r in result:
        yield r['data'] + '\n'


async def lazy_result(count: Union[int, Awaitable[int]],
                      count_cache_key: Hashable,
                      result: AsyncIterable[Any],
                      location: starlette.datastructures.URL,
                      page_size: int,
                      order_key: Sequence[str],
                      order_key_types: Iterable[type],
                      pager: str = 'cursor',
                      offset: int = 0,
                      single: bool = False) -> AsyncGenerator[str, None]:
    prev_link, next_link = None, None
    n = 0
    next_keyset = None
    initial_prefix = '' if single else '{"results":['
    prefix = initial_prefix
    current: Dict[str, Any] = None  # type: ignore
    async for r in result:
        if n == page_size:
            # page is full but there is a next item
            if pager == 'cursor':
                next_keyset = get_keyset(current, order_key)
                next_link = get_page_url(location,
                                         cursor=keyset_serialize(next_keyset, order_key_types))
            elif pager == 'offset':
                prev_link = (get_page_url(location, pager=pager, offset=max(offset - page_size, 0))
                             if offset else None)
                next_link = get_page_url(location, pager=pager, offset=offset + page_size)
        else:
            current = r
            jcurrent = r['data']
            if n > 0:
                prefix = ','
            yield prefix + jcurrent
            n += 1
    if not single:
        if pager != 'cursor' and not isinstance(count, int):
            count = await count
            cached_counts[count_cache_key] = count
        yield ((initial_prefix if current is None else '') +
               (f'],"count":{count}' if pager != 'cursor' else ']') +
               (f',"previous":"{prev_link}"' if prev_link else '') +
               (f',"next":"{next_link}"' if next_link else '') +
               '}')


async def slow_result(count: Union[int, Awaitable[int]],
                      count_cache_key: Hashable,
                      items: Sequence[Any],
                      location: starlette.datastructures.URL,
                      page_size: int,
                      order_key: Sequence[str],
                      order_key_types: Iterable[type],
                      pager: str = 'cursor',
                      offset: int = 0) -> dict:
    if not isinstance(count, int):
        count = await count
        cached_counts[count_cache_key] = count
    result = dict(
        count=count,
        results=[json.loads(r['data'])
                 for r in items[:page_size]],
    )
    if len(items) > page_size:
        if pager == 'cursor':
            keyset = get_keyset(items[-1], order_key)
            result['next'] = get_page_url(location, cursor=keyset_serialize(keyset,
                                                                            types=order_key_types))
        elif pager == 'offset':
            result['next'] = get_page_url(location, pager=pager,
                                          offset=offset + page_size)
    if pager == 'offset' and offset > 0:
        result['previous'] = get_page_url(location, pager=pager,
                                          offset=max(0, offset - page_size))
    return result


async def make_response(db,
                        q: Select,
                        count_q: Select,
                        url,
                        filter_: FilterParams,
                        args: Optional[CommonParams] = None,
                        single: bool = False):
    keyset = None

    if not single:
        assert args
        order_key = [args.sort] + [
            ('-' if args.sort.startswith('-') else '') + name
            for name, c in q.selectable.froms[0].c.items()
            if c.primary_key
        ][:1]

        order_key_types: Iterable[type] = (get_python_type(q.c[k.lstrip('-')].type)
                                           for k in order_key)
        if args.pager == 'cursor':
            order_key_types = list(order_key_types)  # Materialize once

        if args.pager == 'cursor' and args.cursor:
            keyset = keyset_deserialize(args.cursor, order_key_types)
            cleaned_keyset = [(min_value(t) if v is None else v)
                              for v, t in zip(keyset, order_key_types)]
            order_key_tuple = tuple_(*(func.coalesce(q.froms[0].c[k.lstrip('-')], min_value(t))
                                       for k, t in zip(order_key, order_key_types)))
            q = apply_keyset(q,
                             keyset=tuple_(*cleaned_keyset),
                             order_key=order_key_tuple,
                             desc=args.sort.startswith('-'),
                             order_key_types=order_key_types)
        order_by = (q.c[_.lstrip('-')] for _ in order_key)
        q = q.order_by(*((_.desc() for _ in order_by)
                         if args.sort.startswith('-')
                         else order_by))
        if args.pager == 'offset':
            q = apply_offset(q, offset=args.offset)
            args.page_size = args.limit
        if args.format != 'ndjson':
            q = q.limit(args.page_size + 1)

    params = {
        k: (sum((_.split(',') for _ in v), start=[])
            if filter_.__annotations__[k] is Sequence[str]
            else parse_lookup(k)[1].parse_param(v))
        for k, v in filter_.dict(exclude_none=True).items()
    }

    q = q.params(**params)

    # print(q, params)
    # print(str(q).replace('\n', ' '))
    # print(sqlparse.format(str(q.compile(engine)), reindent=True))

    if args and args.format == "ndjson":
        return fastapi.responses.StreamingResponse(ndjson_result(db.iterate(q)),
                                                   media_type="application/x-ndjson")

    if single:
        count: Union[int, Awaitable[int]] = -1
        cached_count_key = ""
    else:
        cached_count_key = get_count_cache_key(url)
        if cached_count_key not in cached_counts:
            count_q = count_q.params(**params)
            # print(count_q)
            # Hopefully the count query will start asynchronously
            count = db.execute(count_q)
        else:
            count = cached_counts[cached_count_key]

    if not single and args and args.slow:
        return await slow_result(count,
                                 cached_count_key,
                                 await db.fetch_all(q),
                                 url,
                                 args.page_size if args else 1,
                                 order_key=order_key,
                                 order_key_types=order_key_types,
                                 pager=args.pager if args else 'cursor',
                                 offset=args.offset if args else 0)

    return fastapi.responses.StreamingResponse(lazy_result(count,
                                                           cached_count_key,
                                                           db.iterate(q),
                                                           url,
                                                           args.page_size if args else 1,
                                                           order_key=() if single else order_key,
                                                           order_key_types=() if single else order_key_types,
                                                           pager=args.pager if args else 'cursor',
                                                           offset=args.offset if args else 0,
                                                           single=single),
                                               media_type="application/json; charset=UTF-8")


def make_route(router: fastapi.APIRouter,
               operation_id_prefix: str,
               path: str,
               config: PgZeroJsonConfig,
               item_model: Type[BaseModel],
               filter_model: Type[FilterParams],
               lookup_param: str = None,
               custom_filters: Dict[str, CustomFilter] = {},
               params_model: Optional[type] = None) -> Tuple[Callable, Callable]:
    get_query = get_query_factory(config, custom_filters)
    if typing.TYPE_CHECKING:
        list_response_model = BaseModel
    else:
        list_response_model = list_response_class_factory(item_model)

    if not params_model:
        class AutoParams(CommonParams):
            sort: str = make_sort_param(config)  # type: ignore

        params_model = dataclasses.dataclass(AutoParams)

    operation_prefix = operation_id_prefix + path.strip('/').replace('/', '_')
    description = getattr(config.table, 'comment', None)

    @router.get(path,
                description=description,
                operation_id=operation_prefix + '_list',
                response_model=list_response_model,
                response_model_exclude_unset=True)
    async def list(
        request: starlette.requests.Request,
        filter_: filter_model = fastapi.Depends(),  # type: ignore
        args: params_model = fastapi.Depends(),  # type: ignore
    ) -> typing.Union[list_response_model, fastapi.responses.StreamingResponse]:  # type: ignore
        count_q, q = get_query(filter_.keys_set())  # type: ignore
        return await make_response(db, q, count_q,
                                   url=request.url,
                                   filter_=filter_,
                                   args=args)

    if not lookup_param:
        return list, list
    lookup_field = item_model.__fields__[lookup_param].field_info

    @router.get("%s{%s}/" % (path, lookup_param),
                description=description,
                operation_id=operation_prefix + '_read',
                response_model=item_model,
                response_model_exclude_unset=True)
    async def read(
        request: starlette.requests.Request,
        lookup_value: str = fastapi.Path(
            ...,
            title=lookup_field.title,
            description=lookup_field.description,
            alias=lookup_param,
        )
    ) -> item_model:  # type: ignore
        count_q, q = get_query((lookup_param,))
        filter_ = filter_model(**{lookup_param: lookup_value})  # type: ignore
        return await make_response(db, q, count_q,
                                   url=request.url,
                                   filter_=filter_,
                                   single=True)

    return list, read


RE_PARAM_WITH_LOOKUP = re.compile(f'__(?:{"|".join(OPS.keys())})$')


def parse_lookup(s: str) -> Tuple[str, Op]:
    name, op = (s.rsplit('__', maxsplit=1) if re.search(RE_PARAM_WITH_LOOKUP, s)
                else (s, 'exact'))
    return name, OPS[op]


def make_join(table: TableCTE, joins: PgZeroJsonJoins) -> FromClause:
    join: FromClause = table
    for left_table, table_joins in joins.items():
        for right_table, left_col, right_col in table_joins:
            join = join.join(right_table,
                             ((left_table.c[left_col] == right_table.c[right_col])
                              if isinstance(right_col, str)
                              else right_col(left_table, right_table)))
    return join


def parse_where(where_params: Iterable[str],
                table: TableCTE,
                cfg: PgZeroJsonConfig) -> Tuple[Dict[Sequence[str], List[ColumnElement]],
                                                PgZeroJsonJoins,
                                                PgZeroJsonJoins]:
    where: Dict[Sequence[str], List[ColumnElement]] = {}
    joins: PgZeroJsonJoins = {}
    result_joins: PgZeroJsonJoins = {}
    for param in where_params:
        name, op = parse_lookup(param)

        extra_joins: PgZeroJsonJoins = {}
        conds: List[ColumnElement] = []
        this_table = table
        many = False
        if name in this_table.c:
            path = [name]
        else:
            path = name.split('__')
        for n, path_el in enumerate(path[:-1]):
            fk_name = path_el + '_id'
            if fk_name in this_table.c:
                fk_col = next(iter(this_table.c[fk_name].foreign_keys)).column
                that_table = this_table
                this_table = fk_col.table
                extra_joins.setdefault(that_table, []).append((this_table, fk_name, fk_col.name))
            else:
                if isinstance(this_table, CTE):
                    selectable = this_table.element
                    real_table = selectable._froms[0]  # type: ignore
                else:
                    real_table = this_table
                try:
                    target_table_name = next(
                        (k[:-1] for k, v in cfg.rename_relationships[real_table].items()
                         if v == path_el)
                    )
                except (StopIteration, KeyError):
                    raise Exception(f"{path_el} not found in {real_table}")

                that_table = this_table
                this_table = cfg.metadata.tables[target_table_name]
                referenced_by = list(get_referenced_by(real_table, cfg.metadata, set()))
                c_pid = next(c for t, c, _ in referenced_by if t == this_table)
                extra_joins.setdefault(that_table, []).append(
                    (this_table, list(real_table.primary_key)[0].name, c_pid.name)
                )
                many = True
        if path[-1] in this_table.c:
            column_name = path[-1]
            this_column = this_table.c[column_name]
            this_column_type = get_python_type(this_column.type)
        elif path[-1] + '_id' in this_table.c:
            column_name = path[-1] + '_id'
            this_column = this_table.c[column_name]
            this_column_type = get_python_type(this_column.type)
        elif path[-1] in (name for name, *_ in cfg.add_fields.get(this_table, [])):
            column_name, this_column, (this_column_type, _) = \
                next(_ for _ in cfg.add_fields[this_table] if _[0] == path[-1])
            joins.setdefault(this_table, []).extend(cfg.add_joins.get(this_table, []))
        else:
            raise Exception(f"{path[-1]} not found in {this_table}")
        if issubclass(this_column_type, list):
            if op.op is operator.eq:
                if op.expr:
                    conds.append(this_column
                                 .overlap(column(column_name)))  # type: ignore
                else:
                    conds.append(bindparam(param) == func.any(column(column_name)))
            else:
                raise Exception(
                    f"{op} not supported for multivalued column {this_table}.{column_name}"
                )
        else:
            conds.append(op.make_expr(column(column_name), param))

        if many:
            subquery_joins: PgZeroJsonJoins = {}
            subquery_table: Optional[TableCTE] = None
            for left_table, table_joins in extra_joins.items():
                if left_table == table:
                    subquery_table = table_joins[0][0]
                    for right_table, left_col, right_col in table_joins:
                        conds.append(left_table.c[left_col] == right_table.c[right_col])
                else:
                    subquery_joins[left_table] = table_joins
            assert subquery_table is not None
            q = select().select_from(make_join(subquery_table, subquery_joins)).where(and_(*conds))
            where.setdefault((), []).append(exists(q))
        else:
            where.setdefault((), []).extend(conds)
            for k, v in extra_joins.items():
                result_joins.setdefault(k, []).extend(v)
    return where, joins, result_joins


def querystring_replace(query_string: str, **kwargs) -> str:
    return urlencode(list(chain(
        ((k, v) for k, v in parse_qsl(query_string)
         if k not in kwargs),
        ((k, v) for k, v in kwargs.items() if v is not None),
    )))
