from setuptools import setup


setup(name='pgzerojsonapi',
      version='0.4',
      description='API framework based on FastAPI and pgzerojson',
      author='Phil Krylov',
      author_email='phil@krylov.eu',
      license='MIT',
      packages=['pgzerojsonapi'],
      python_requires='>=3.9',
      install_requires=[
          'databases[postgresql]<0.5',
          'fastapi',
          'orjson',
          'pgzerojson>=0.4',
          'psycopg2-binary<2.9',
          'sqlalchemy<1.4',
          'temporenc',
      ],
      zip_safe=False)
